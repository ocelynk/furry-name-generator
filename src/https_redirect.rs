
use std::str;

use iron::prelude::*;
use iron::AroundMiddleware;
use iron::Handler;
use iron::status;
use iron::headers::Location;
use iron::modifiers::Header;

use url;

/// Iron middleware that redirects requests from HTTP to HTTPS
///
/// The protocol is based on the X-Forwarded-Proto header if it exists, or on the URL scheme
/// otherwise.
pub struct HttpsRedirect;

impl AroundMiddleware for HttpsRedirect {
    fn around(self, handler: Box<Handler>) -> Box<Handler> {
        let redirect_handler = move |request: &mut Request| {
            let protocol = get_x_forwarded_proto(&request)
                .unwrap_or_else(|| request.url.scheme().to_owned());

            match &*protocol {
                "http" => {
                    // 301 redirect to HTTPS
                    let mut https_url: url::Url = request.url.clone().into();
                    https_url.set_scheme("https").expect("Can't set scheme to HTTPS");
                    Ok(Response::with((
                        status::MovedPermanently,
                        Header(Location(https_url.to_string()))
                    )))
                }
                "https" => {
                    // Handle as usual
                    handler.handle(request)
                }
                _ => {
                    // What scheme is this? something not supported
                    Ok(Response::with(status::BadRequest))
                }
            }
        };
        Box::new(redirect_handler)
    }
}

/// Returns the value of the first X-Forwarded-Proto header, if it exists
/// and is valid UTF-8
fn get_x_forwarded_proto<'r>(request: &'r Request) -> Option<String> {
    request.headers.get_raw("X-Forwarded-Proto")
        .and_then(|values| values.first())
        .and_then(|value| str::from_utf8(&value).ok())
        .map(String::from)
}
