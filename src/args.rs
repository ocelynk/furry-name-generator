use std::net::SocketAddr;
use std::path::PathBuf;

use clap::{App, Arg};

/// Command-line arguments
pub struct Args {
    /// Local address to bind to
    pub bind_address: SocketAddr,
    /// Path to the data folder
    pub data_path: PathBuf,
    /// Log folder path
    pub log_path: PathBuf,
    /// Cache time to live for static files, seconds
    pub cache_ttl: u64,
    /// Redirects all HTTP requests to HTTPS
    pub redirect_to_https: bool,
}

impl Args {
    pub fn get() -> Self {
        let matches = App::new(crate_name!())
            .version(crate_version!())
            .about(crate_description!())
            .author(crate_authors!())
            .arg(
                Arg::with_name("bind_address")
                    .takes_value(true)
                    .default_value("127.0.0.1:8080")
                    .long("bind-address")
                    .help("The local address to bind to")
                    .validator(|address| {
                        address
                            .parse::<SocketAddr>()
                            .map(|_| ())
                            .map_err(|e| e.to_string())
                    }),
            ).arg(
                Arg::with_name("data_path")
                    .takes_value(true)
                    .required(true)
                    .long("data-path")
                    .help("The path to the data folder"),
            )
            .arg(Arg::with_name("log_path")
                .takes_value(true)
                .long("log-path")
                .help("A directory to write log files to. If this is not specified, log files will be placed in the data folder.")
            )
            .arg(
                Arg::with_name("cache_ttl")
                    .takes_value(true)
                    .default_value("86400")
                    .long("ttl")
                    .help("Cache time to live for static resources, seconds")
                    .validator(|ttl| ttl.parse::<u64>().map(|_| ()).map_err(|e| e.to_string())),
            )
            .arg(
                Arg::with_name("redirect_to_https")
                    .long("redirect-to-https")
                    .help("Causes all incoming HTTP requests to be redirected to HTTPS using 301 permanent redirects")
            )
            .get_matches();

        let data_path = PathBuf::from(matches.value_of_os("data_path").unwrap());
        let log_path = matches
            .value_of_os("log_path")
            .map(PathBuf::from)
            .unwrap_or_else(|| data_path.clone());

        Args {
            bind_address: matches.value_of("bind_address").unwrap().parse().unwrap(),
            data_path,
            log_path,
            cache_ttl: matches.value_of("cache_ttl").unwrap().parse().unwrap(),
            redirect_to_https: matches.is_present("redirect_to_https"),
        }
    }
}
