extern crate iron;
extern crate persistent;
extern crate staticfile;
extern crate urlencoded;
#[macro_use]
extern crate router;
extern crate simplelog;
#[macro_use]
extern crate clap;
extern crate url;

mod args;
mod subprocess;
mod https_redirect;

use args::Args;
use subprocess::Subprocess;
use https_redirect::HttpsRedirect;

use std::fs::OpenOptions;
use std::path::{Path, PathBuf};
use std::time::Duration;

use iron::method::Method;
use iron::prelude::*;
use iron::status::Status;
use iron::typemap::Key;
use persistent::Write;
use staticfile::Static;
use urlencoded::UrlEncodedQuery;

struct SubprocessKey;

impl Key for SubprocessKey {
    type Value = Subprocess;
}

fn temperature_param(request: &mut Request) -> Option<f32> {
    request
        .get_ref::<UrlEncodedQuery>()
        .ok()
        .and_then(|map| map.get("temperature"))
        .and_then(|values| values.first())
        .and_then(|value| value.parse().ok())
}

fn generate_name_handler(request: &mut Request) -> IronResult<Response> {
    // Handle head
    if request.method == Method::Head {
        Ok(Response::with(Status::Ok))
    } else {
        let temperature = temperature_param(request).unwrap_or(0.5);

        let textgen = request
            .extensions
            .get::<Write<SubprocessKey>>()
            .expect("No subprocess");

        // Get suprocess out of Lazy
        let mut textgen = textgen
            .lock()
            .unwrap_or_else(|e| {
                // Disregard poisoning, get the lock anyway
                e.into_inner()
            });

        let name = textgen
            .generate(temperature)
            .map_err(|e| IronError::new(e, Status::InternalServerError))?;

        Ok(Response::with((Status::Ok, name)))
    }
}

fn make_static_files(path: PathBuf, ttl: Duration) -> Static {
    Static::new(path).cache(ttl)
}

fn main() -> Result<(), Box<std::error::Error>> {
    let args = Args::get();
    log_init(&args.log_path).unwrap_or_else(|e| {
        eprintln!("Logging setup failed: {}", e);
    });

    let subprocess = Subprocess::start(&args.data_path)?;
    let subprocess: Write<SubprocessKey> = Write::one(subprocess);

    let mut generate_chain = Chain::new(generate_name_handler);
    generate_chain.link_before(subprocess);

    let cache_duration = Duration::from_secs(args.cache_ttl);
    let static_files = make_static_files(args.data_path.join("web"), cache_duration);

    let mut router_chain = Chain::new(router! {
        generate: get "/generate" => generate_chain,
        index: get "/" => static_files.clone(),
        static_files: get "*" => static_files.clone(),

        index_head: head "/" => static_files.clone(),
        static_files_head: head "*" => static_files
    });

    if args.redirect_to_https {
        router_chain.link_around(HttpsRedirect);
    }

    Iron::new(router_chain).http(args.bind_address)?;
    Ok(())
}

/// Sets up logging
fn log_init(log_path: &Path) -> Result<(), Box<std::error::Error>> {
    use simplelog::{CombinedLogger, Config, LevelFilter, SharedLogger, TermLogger, WriteLogger};

    let log_file = OpenOptions::new()
        .create(true)
        .append(true)
        .open(log_path.join("log.txt"))?;

    // Full ISO 8601 date/time format for log file
    let mut file_config = Config::default();
    file_config.time_format = Some("%+");

    let file_logger = WriteLogger::new(LevelFilter::Warn, file_config, log_file);
    let mut loggers: Vec<Box<SharedLogger>> = vec![file_logger];
    // Try to set up terminal logging, but ignore if not possible
    if let Some(term_logger) = TermLogger::new(LevelFilter::Warn, Config::default()) {
        loggers.push(term_logger);
    }

    CombinedLogger::init(loggers)?;

    Ok(())
}
