use std::io::{self, BufRead, BufReader, Error, ErrorKind, Lines, Write};
use std::path::Path;
use std::process::{Child, ChildStdin, ChildStdout, Command, Stdio};

/// Manages a Python subprocess that does the actual neural network tasks
pub struct Subprocess {
    /// Child process
    child: Child,
    /// Child input
    child_in: ChildStdin,
    /// Child output line iterator
    child_lines: Lines<BufReader<ChildStdout>>,
}

impl Subprocess {
    pub fn start(data_folder: &Path) -> io::Result<Self> {
        let script_path = data_folder.join("python/furry_name_generator_subprocess.py");

        let mut child = Command::new("python3")
            .arg(script_path)
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .spawn()?;

        let child_out = child.stdout.take().unwrap();
        let child_in = child.stdin.take().unwrap();

        Ok(Subprocess {
            child,
            child_in,
            child_lines: BufReader::new(child_out).lines(),
        })
    }

    pub fn generate(&mut self, temperature: f32) -> io::Result<String> {
        writeln!(self.child_in, "{}", temperature)?;
        self.child_lines
            .next()
            .ok_or_else(|| Error::new(ErrorKind::UnexpectedEof, "Child output ended"))?
    }
}

impl Drop for Subprocess {
    fn drop(&mut self) {
        let _ = self.child.kill();
        let _ = self.child.wait();
    }
}
