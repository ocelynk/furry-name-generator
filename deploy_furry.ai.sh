#!/bin/bash

# Applies the furry.ai patch and deploys the application to Heroku
# Assumes that all the Heroku things are already set up

if [[ -n $(git status -s) ]]; then
    echo 'Commit, stash, or ignore all changes before deploying'
    exit -1
fi

git checkout -b deploy
git apply furry.ai.patch
git add data/web/index.html data/web/js/name_generator.js
git commit -m 'furry.ai patches'
git push --force heroku deploy:master

# Clean up
git checkout master
git branch -D deploy
