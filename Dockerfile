# Builds an image that runs the furry_name_server application

FROM debian:sid

# Packages to install: python3 python3-pip curl
# pip3 install tensorflow textgenrnn==1.4

RUN apt-get update && \
    apt-get install -y python3 python3-pip curl && \
    pip3 install tensorflow textgenrnn==1.4 && \
    curl https://sh.rustup.rs -sSf | sh -s -- -y

RUN mkdir /root/furry_name_generator
COPY . /root/furry_name_generator/

# Build
RUN cd /root/furry_name_generator && \
    /root/.cargo/bin/cargo build --release

# furry_name_server listens on port 8080
EXPOSE 8080/tcp

# Entry point
ENTRYPOINT ["/root/furry_name_generator/target/release/furry_name_server", "--bind-address", "0.0.0.0:8080", "--data-path", "/root/furry_name_generator/data"]
