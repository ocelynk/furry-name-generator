# Furry Name Generator

This web application uses a neural network to generate names for furry characters.

## Setup

### Prerequisites

* Python 3
* Python packages `tensorflow` and `textgenrnn` (can be installed using `pip3`)
* A recent stable version of the Rust compiler and Cargo

### Compiling

Simply `cargo build` (for production builds, `cargo build --release`)

### Installing

The furry_name_generator executable will be created in `target/debug` or `target/release`
depending on the build type. Copy it into some convenient place.

Also copy the `data` folder somewhere. When running the server, you will
give it the path to the data folder.

### Running

```
furry_name_server 0.1.0
Ocelynk <ocelynk@ocelynk.com>
Web server for the furry name generator web application

USAGE:
    furry_name_server [FLAGS] [OPTIONS] --data-path <data_path>

FLAGS:
    -h, --help                 Prints help information
        --redirect-to-https    Causes all incoming HTTP requests to be redirected to HTTPS using 301 permanent redirects
    -V, --version              Prints version information

OPTIONS:
        --bind-address <bind_address>    The local address to bind to [default: 127.0.0.1:8080]
        --ttl <cache_ttl>                Cache time to live for static resources, seconds [default: 86400]
        --data-path <data_path>          The path to the data folder
        --log-path <log_path>            A directory to write log files to. If this is not specified, log files will be
                                         placed in the data folder.
```

The only required argument is `data-path`, which should be the absolute or
relative path to the `data` folder.

The default `bind-address` will only accept connections from localhost.
In production, the IP address should be changed to `0.0.0.0` or the address
of a network interface.

## License

Licensed under either of

 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you shall be dual licensed as above, without any
additional terms or conditions.
