
document.addEventListener("DOMContentLoaded", function(event) {

    // Load UI
    let temperatureSlider = document.getElementById('temperatureSlider');
    let generateButton = document.getElementById('generateButton');
    let nameText = document.getElementById('nameText');
    let progressBar = document.getElementById('progressBar');

    // Expected time before the server responds
    let expectedResponseTime = 5.0;


    let ProgressBarAnimator = function(bar) {
        this.bar = bar;
    };
    ProgressBarAnimator.prototype.start = function(duration) {
        // Set up time
        this.startTime = new Date();
        this.endTime = new Date(this.startTime.getTime() + duration * 10000)

        let thisAnimator = this;
        this.interval = setInterval(function() { thisAnimator.updateProgress() }, 100);
    };
    ProgressBarAnimator.prototype.updateProgress = function() {
        let now = new Date();
        if (now.getTime() >= this.endTime.getTime()) {
            // Done
            this.cancel();
            this.setProgress(1);
        } else {
            // Interpolate
            let span = this.endTime.getTime() - this.startTime.getTime();
            let elapsed = now.getTime() - this.startTime.getTime();
            this.setProgress(elapsed / span);
        }
    };
    ProgressBarAnimator.prototype.setProgress = function(ratio) {
        let percent = ratio * 100.0;
        this.bar.style.width = percent + '%';
        this.bar.setAttribute('aria-valuenow', percent);
    };
    ProgressBarAnimator.prototype.cancel = function() {
        if (this.interval) {
            clearInterval(this.interval);
            this.interval = null;
        }
    }

    let generateName = function() {
        // Disable UI
        generateButton.disabled = true;
        let animator = new ProgressBarAnimator(progressBar);
        animator.start(expectedResponseTime);

        let url = 'generate?temperature=' + temperatureSlider.value;
        let xhr = new XMLHttpRequest();

        xhr.onload = function() {
            if (xhr.status == 200) {
                nameText.textContent = xhr.response;
                animator.cancel();
                animator.setProgress(1);
            } else {
                alert('Unexpected status ' + xhr.status);
                animator.cancel();
                animator.setProgress(0);
            }
            generateButton.disabled = false;
        }
        xhr.onerror = function() {
            alert('Network error');
            generateButton.disabled = false;
            animator.cancel();
            animator.setProgress(0);
        }

        xhr.open('GET', url);
        xhr.send();
    };

    generateButton.onclick = function() {
        generateName();
    };

    // Generate the first name to get started
    nameText.textContent = "Generating name...";
    generateName();

});
