#!/usr/bin/env python3

import os
import sqlite3

from textgenrnn import textgenrnn

DATA_DIR = os.path.dirname(__file__) + '/..'
CONFIG_PATH = DATA_DIR + '/textgenrnn_config.json'
VOCAB_PATH = DATA_DIR + '/textgenrnn_vocab.json'
WEIGHTS_PATH = DATA_DIR + '/textgenrnn_weights.hdf5'

TAGS_PATH = DATA_DIR + '/tags_character.sqlite3'

class NameGenerator(object):

    def __init__(self, tags_path, weights_path, vocab_path, config_path):
        db = sqlite3.connect(tags_path)
        tags = db.execute('SELECT name FROM tags')
        self.tags = [row[0] for row in tags]
        db.close()

        self.tg = textgenrnn(weights_path=weights_path,
                               vocab_path=vocab_path,
                               config_path=config_path)

        self.temperature = 1.0

    def set_temperature(self, temperature):
        self.temperature = temperature

    def generate(self):
        return self.tg.generate(temperature = self.temperature, return_as_list = True)[0]

    def generate_non_plagarized(self):
        while True:
            generated = self.generate()
            if generated not in self.tags:
                return generated

    def prompt(self):
        command = input('')
        self.temperature = float(command)
        print(postprocess_name(self.generate_non_plagarized()))


def main():
    generator = NameGenerator(TAGS_PATH, WEIGHTS_PATH, VOCAB_PATH, CONFIG_PATH)
    try:
        while True:
            generator.prompt()
    except KeyboardInterrupt:
        pass
    except EOFError:
        pass

# Processes a name, removing underscores and applying title case
def postprocess_name(name):
    name = name.replace('_', ' ')
    name = name.title()
    return name

if __name__ == "__main__":
    main()
